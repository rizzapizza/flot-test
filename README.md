## How to install and set-up

1. Install **Python 2.7**.
2. Get **easy_install**.
3. Get **pip** by typing the following command on terminal: `easy_install pip`.
4. Install virtual environment by typing the following command on terminal: `pip install virtualenv`.
5. Create a new directory to serve as your workspace.
6. Change your working directory to the new directory.
7. Type `virtualenv venv` on the terminal window.
8. Now, activate the virtual environment. On windows: type `venv\Scripts\activate.bat`. On Linux/Mac: type `venv/bin/activate`. Take note that the windows command uses back slashes while the other command uses forward slashes.
9. Clone this repository on your workspace.
10. Change your current directory to the cloned repository.
11. Type `pip install -r requirements.txt` and wait for the installation of dependencies to finish.
12. Run the server by typing `python run.py`.
13. Go to your browser and access: **localhost:5000**. The GUI should be there.
