from app import app
from flask import render_template
from flask import jsonify

# from custom import DataFetcher, TIDDataFetcher
from custom import SpecificDataFetcher

DATA_LENGTH = 10

# data_fetchers = [DataFetcher(DATA_LENGTH, "app/data/sensor_data1.csv"), DataFetcher(DATA_LENGTH, "app/data/sensor_data2.csv")]
# tid_data_fetchers = [TIDDataFetcher(DATA_LENGTH, "app/data/sensor_data2.csv"), TIDDataFetcher(DATA_LENGTH, "app/data/sensor_data2.csv")]
specific_data_fetcher = SpecificDataFetcher(DATA_LENGTH, "app/data/sensor_data1.csv")

@app.route('/')
def show_gui():
    return render_template('gui.html')

@app.route('/get_sensor_data/<multiplier>/<resolution>')
def get_sensor_data(multiplier, resolution):
    try:
        multiplier = int(multiplier)
    except:
        return jsonify([])

    if multiplier >= 0:
        print("Multiplier: " + str(multiplier))
        print("Resolution: " + str(resolution))

        json_result = jsonify(specific_data_fetcher.getNextData(multiplier, resolution))

        print("JSON Result")
        print(json_result)

        return json_result
    else:
        return jsonify([])

'''@app.route('/basic_plot')
def show_basic_plot():
    return render_template('basic_plot.html')

@app.route('/random_realtime')
def show_random_realtime_plot():
    return render_template('random_realtime.html')

@app.route('/single_series_json_realtime1')
def show_single_series_plot1():
    return render_template('single_series_json_realtime1.html') # using global variables

@app.route('/single_series_json_realtime2')
def show_single_series_plot2():
    return render_template('single_series_json_realtime2.html') # using callbacks

@app.route('/multiple_series_json_realtime1')
def show_multiple_series_plot1():
    return render_template('multiple_series_json_realtime1.html')

@app.route('/multiple_series_json_realtime2')
def show_multiple_series_plot2():
    return render_template('multiple_series_json_realtime2.html')

@app.route('/multiple_areas')
def show_multiple_areas():
    return render_template('multiple_areas.html')

@app.route('/get_sensor_data1/<sensor_data_num>')
def get_sensor_data_in_json1(sensor_data_num):
    if sensor_data_num:
        sensor_data_num = int(sensor_data_num)

        if sensor_data_num > 0 and sensor_data_num <= len(data_fetchers):
            print("Sensor Data Number: " + str(sensor_data_num))

            json_result = jsonify(data_fetchers[sensor_data_num - 1].getNextData())

            print("JSON Result")
            print(json_result)

            return json_result
        else:
            return jsonify([])
    else:
        return jsonify([])

@app.route('/get_sensor_data2/<sensor_data_num>')
def get_sensor_data_in_json2(sensor_data_num):
    if sensor_data_num:
        sensor_data_num = int(sensor_data_num)

        if sensor_data_num > 0 and sensor_data_num <= len(data_fetchers):
            print("Sensor Data Number: " + str(sensor_data_num))

            json_result = jsonify(tid_data_fetchers[sensor_data_num - 1].getNextData())

            print("JSON Result")
            print(json_result)

            return json_result
        else:
            return jsonify([])
    else:
        return jsonify([])

@app.route('/bootstrap_test')
def run_bootstrap_test():
    return render_template('bootstrap_test.html')

@app.route('/bootstrap_multiple_areas')
def bootstrap_multiple_areas():
    return render_template('bootstrap_multiple_areas.html')

@app.route('/toggle_series')
def toggle_series():
    return render_template('toggle_series.html')'''
