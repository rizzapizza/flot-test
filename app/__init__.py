from flask import Flask
from flask_bootstrap import Bootstrap

app = Flask(__name__) # instance of flask
Bootstrap(app)
from app import views # app is a module here
