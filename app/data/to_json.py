from collections import OrderedDict

import json
import csv

filenames = ["sensor_data1.csv", "sensor_data2.csv"]

for name in filenames:
    csv_file = open(name)
    rows = csv.DictReader(csv_file)

    csv_data = []
    for row in rows:
        csv_data.append(row)

    csv_file.close()

    results = {}
    for i in range(0, len(csv_data)):
        tid = csv_data[i]['tid']
        src = csv_data[i]['src']
        ts = csv_data[i]['ts']
        sdata = csv_data[i]['sdata']

        if not(tid in results):
            results[tid] = {}

        if not(src in results[tid]):
            results[tid][src] = []

        new_data = {'ts':ts, 'sdata':sdata} # added in order, anyway
        results[tid][src].append(new_data)

    results = OrderedDict(sorted(results.items())) # sort result contents by tid

    for tid in results: # sort tid contents by src
        results[tid] = OrderedDict(sorted(results[tid].items()))

    parsed = name.split(".")
    json_name = parsed[0] + ".json"
    with open(json_name, 'w') as outfile:
        json.dump(results, outfile, sort_keys = True, indent = 4, ensure_ascii = False)
