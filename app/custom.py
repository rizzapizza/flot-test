from collections import OrderedDict
import csv

class DataFetcher(object):
    def __init__(self, max_data_length, filename):
        self.row_count = 0
        self.max_data_length = max_data_length

        try:
            csv_file = open(filename)
            rows = csv.DictReader(csv_file)

            self.csv_data = []
            for row in rows:
                self.csv_data.append(row)

            csv_file.close()
        except:
            print "The file %s could not be opened." % filename

    def getTerminators(self):
        if self.row_count < len(self.csv_data):
            start = self.row_count
            if (self.row_count + self.max_data_length) <= len(self.csv_data):
                stop = self.row_count + self.max_data_length
                self.row_count += self.max_data_length
            else:
                stop = len(self.csv_data)
                self.row_count = 0
        else:
            start = 0
            if (self.row_count + self.max_data_length) <= len(self.csv_data):
                stop = self.max_data_length
                self.row_count = self.max_data_length
            else:
                stop = len(self.csv_data)
                self.row_count = 0

        return start, stop

    def getNextData(self):
        start, stop = self.getTerminators()

        results = OrderedDict()
        for i in range(start, stop):
            if not(self.csv_data[i]['src'] in results):
                results[self.csv_data[i]['src']] = []

            new_data = {'ts': self.csv_data[i]['ts'], 'sdata': self.csv_data[i]['sdata']}
            results[self.csv_data[i]['src']].append(new_data)

        return results

class TIDDataFetcher(DataFetcher):
    def getNextData(self):
        start, stop = self.getTerminators()

        results = {}
        for i in range(start, stop):
            tid = self.csv_data[i]['tid']
            src = self.csv_data[i]['src']
            ts = self.csv_data[i]['ts']
            sdata = self.csv_data[i]['sdata']

            if not(tid in results):
                results[tid] = {}

            if not(src in results[tid]):
                results[tid][src] = []

            new_data = {'ts':ts, 'sdata':sdata} # added in order, anyway
            results[tid][src].append(new_data)

        results = OrderedDict(sorted(results.items())) # sort result contents by tid

        for tid in results: # sort tid contents by src
            results[tid] = OrderedDict(sorted(results[tid].items()))

        return results

class SpecificDataFetcher(DataFetcher):
    def getNextData(self, multiplier, resolution):
        results = {}

        start = multiplier * self.max_data_length
        stop = (multiplier + 1) * self.max_data_length
        if stop > len(self.csv_data):
            stop = len(self.csv_data)

        for i in range(start, stop):
            tid = self.csv_data[i]['tid']
            src = self.csv_data[i]['src']
            ts = self.csv_data[i]['ts']
            sdata = self.csv_data[i]['sdata']

            if not(tid in results):
                results[tid] = {}

            if not(src in results[tid]):
                results[tid][src] = []

            new_data = {'ts':ts, 'sdata':sdata} # added in order, anyway
            results[tid][src].append(new_data)

        results = OrderedDict(sorted(results.items())) # sort result contents by tid

        for tid in results: # sort tid contents by src
            results[tid] = OrderedDict(sorted(results[tid].items()))

        return results
